import {Component} from "@angular/core";


@Component({
  selector: 'app-seek-find-input',
  template: '<router-outlet></router-outlet>'
})
export class SeekFindInputComponent {}
