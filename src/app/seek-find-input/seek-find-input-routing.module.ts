import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {SeekFindInputComponent} from "./seek-find-input.component";
import {CodeListViewComponent} from "./code-list-view/code-list-view.component";

const routes: Routes = [
  {
    path: '',
    component: SeekFindInputComponent,
    children: [
      {path: '', component: CodeListViewComponent},
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SeekFindInputRoutingModule {}
