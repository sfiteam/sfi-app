export interface SeekFindInputCode {
  id?: string
  comment: string
  points: number
  used: boolean
  active: boolean
}
