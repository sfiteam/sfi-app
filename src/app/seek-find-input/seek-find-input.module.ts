import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SeekFindInputComponent} from "./seek-find-input.component";
import {SharedModule} from "../shared/shared.module";
import {FormsModule} from "@angular/forms";
import {NgxAuthFirebaseUIModule} from 'ngx-auth-firebaseui';
import {SeekFindInputRoutingModule} from "./seek-find-input-routing.module";
import { CodeListViewComponent } from './code-list-view/code-list-view.component';
import {NgxQRCodeModule} from "ngx-qrcode2";
import {MatExpansionModule} from "@angular/material";

@NgModule({
  declarations: [SeekFindInputComponent, CodeListViewComponent],
  imports: [
    CommonModule,
    SeekFindInputRoutingModule,
    FormsModule,
    NgxQRCodeModule,
    SharedModule,
    NgxAuthFirebaseUIModule,
    MatExpansionModule
  ],
  entryComponents: []
})
export class SeekFindInputModule { }
