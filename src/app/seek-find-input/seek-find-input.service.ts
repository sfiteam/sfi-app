import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {SeekFindInputCode} from "./seek-find-input-code";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class SeekFindInputService {

  codesCollection: AngularFirestoreCollection;

  constructor(public afs: AngularFirestore) {
    this.codesCollection = this.afs.collection('seekFindInputCodes');
  }

  public async addBatch(comment: string, points: number, quantity: number) {

    const newCode: SeekFindInputCode = {
      comment: comment,
      points: points,
      used: false,
      active: true
    };
    Array.from({length: quantity}).forEach(async () => await this.codesCollection.add(newCode));
  }

  public getChanges() {
    return this.codesCollection.snapshotChanges().pipe(map(changes => {
      return changes.map(change => ({
        id: change.payload.doc.id,
        ...change.payload.doc.data()
      }));
    }));
  }

}
