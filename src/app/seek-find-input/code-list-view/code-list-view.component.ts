import {Component, OnInit} from '@angular/core';
import {SeekFindInputService} from "../seek-find-input.service";
import {SeekFindInputCode} from "../seek-find-input-code";

@Component({
  selector: 'app-code-list-view',
  templateUrl: './code-list-view.component.html',
  styleUrls: ['./code-list-view.component.scss']
})
export class CodeListViewComponent implements OnInit {

  groupedCodes: any[] = [];

  generateOptions = {
    comment: '',
    points: null,
    quantity: null
  };

  constructor(private seekFindInputService: SeekFindInputService) {
  }

  ngOnInit() {
    this.seekFindInputService.getChanges().subscribe((data) => {
      this.groupedCodes = Object.values(data.reduce((acc, cur: any) => {
        const key = cur.comment + '|' + cur.points;
        if(!acc.hasOwnProperty(key)){
          acc[key] = {
            comment: cur.comment,
            points: cur.points,
            codes: []
          };
        }
        acc[key].codes.push(cur);
        return acc;
      }, {}));
    });
  }

  getTooltipInfo(code: SeekFindInputCode) {
    return `ID: ${code.id}, Used: ${code.used}, Active: ${code.active}`
  }

  async generateCode() {

    await this.seekFindInputService.addBatch(
      this.generateOptions.comment, this.generateOptions.points, this.generateOptions.quantity
    );

    this.generateOptions = {comment: '', quantity: null, points: null}
  }

}
