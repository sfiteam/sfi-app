import { Component, OnInit } from '@angular/core';
import {DashboardService} from "../../charts/dashboard.service";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  lectureCount: number = 0;
  averageRating: string = "0";
  ratingsCollected: number = 0;

  codesCount: number = 0;
  usedCodesCount: number = 0;
  notUsedCodesCount: number = 0;

  constructor(public dashboardService: DashboardService) { }

  ngOnInit() {
    this.dashboardService.getAllLectures().subscribe((data) => {
      this.lectureCount = data.length;
      const ratings = data.reduce((acc, cur) => [...acc, ...cur.rating], []);
      this.ratingsCollected = ratings.length;
      if(this.ratingsCollected > 0){
        this.averageRating = (ratings.reduce((acc, cur) => acc + cur, 0) / this.ratingsCollected).toFixed(2);
      }
    });

    this.dashboardService.getAllCodes().subscribe(data => {
      this.codesCount = data.length;
      this.usedCodesCount = data.filter(x => x.used).length;
      this.notUsedCodesCount = this.codesCount - this.usedCodesCount;
    });
  }

}
