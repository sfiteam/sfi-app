import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import {RouterModule} from "@angular/router";
import {CoreRoutingModule} from "./core-routing.module";
import {SharedModule} from "../shared/shared.module";
import {AuthComponent} from '../auth/auth.component';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';
import { RatingChartComponent } from '../charts/rating-chart/rating-chart.component';
import { CounterCardComponent } from '../charts/counter-card/counter-card.component';

@NgModule({
  declarations: [
    HeaderComponent,
    HomeComponent,
    AuthComponent,
    RatingChartComponent,
    CounterCardComponent
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    SharedModule,
    NgxAuthFirebaseUIModule,
  ],
  exports: [
    RouterModule,
    HeaderComponent,
    HomeComponent,
    RatingChartComponent,
  ]
})
export class CoreModule { }
