import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {MediaMatcher} from "@angular/cdk/layout";
import {AuthService} from 'src/app/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {


  mobileQuery: MediaQueryList;
  title: string = "SFI";

  menuItems = [
    {"name": "Dashboard", "href": "/", "icon": "poll"},
    {"name": "Agenda", "href": "/agenda", "icon": "calendar_today"},
    {"name": "Seek Find Input", "href": "/seek-find-input", "icon": "camera"},
    {"name": "Organizers", "href": "/organizers", "icon": "person"},
  ];


  private readonly _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, public auth: AuthService) {
    this.mobileQuery = media.matchMedia('(max-width: 900px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

}
