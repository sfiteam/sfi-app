import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {AuthGuard} from './auth.guard';
import {AuthComponent} from '../auth/auth.component';

const routes: Routes = [
  {
    path: "agenda",
    loadChildren: '../agenda/agenda.module#AgendaModule',
    canActivate: [AuthGuard]
  },
  {
    path: "seek-find-input",
    loadChildren: '../seek-find-input/seek-find-input.module#SeekFindInputModule',
    canActivate: [AuthGuard]
  },
  {
    path: "organizers",
    loadChildren: '../organizers/organizers.module#OrganizersModule',
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: AuthComponent
  }
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class CoreRoutingModule {
}
