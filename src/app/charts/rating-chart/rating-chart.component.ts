import {Component, NgModule, OnInit} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {NgxChartsModule, count} from '@swimlane/ngx-charts';
import {DashboardService} from '../dashboard.service';

@Component({
  selector: 'app-rating-chart',
  templateUrl: './rating-chart.component.html',
  styleUrls: ['./rating-chart.component.scss']
})
export class RatingChartComponent implements OnInit {

  view: any[] = [700, 400];
  lecturesData: any[];

  chartOptions = {
    showXAxis: true,
    showYAxis: true,
    gradient: false,
    showLegend: true,
    showXAxisLabel: true,
    xAxisLabel: 'Lecture',
    showYAxisLabel: true,
    yAxisLabel: 'Average rating',
  };

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor(private dashboardService: DashboardService) {
  }

  ngOnInit() {
    this.dashboardService.getAllLectures().subscribe((results) => {
      this.processData(results);
    });
  }

  onSelect(event) {
  }

  processData(entries) {

    this.lecturesData = [];

    entries.forEach(element => {

      if (element.rating.length === 0) {
        return;
      }

      const countRatings = element.rating.length;
      let sumRatings = 0;

      for (let i = 0; i < countRatings; i++) {

        sumRatings = sumRatings + element.rating[i];

      }

      const average = sumRatings / countRatings;

      const singleEntry = {
        name: element.title,
        value: average
      };

      this.lecturesData.push(singleEntry);

    });
  }


}
