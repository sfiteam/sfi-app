import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
 
@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private afs: AngularFirestore) { }

  getAllLectures(): Observable<any> {
    return this.afs.collection('lectures').valueChanges();
  }

  getAllUsers(): Observable<any> {
    return this.afs.collection('users').valueChanges();
  }

  getAllCodes(): Observable<any> {
    return this.afs.collection("seekFindInputCodes").valueChanges();
  }

}
