import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {OrganizerItem} from './organizer-item';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrganizersService {

  organizerItemsCollection: AngularFirestoreCollection<OrganizerItem>;

  constructor(public afs: AngularFirestore) {
    this.organizerItemsCollection = this.afs.collection('organizators');
  }

  getItems() {
    return this.organizerItemsCollection.snapshotChanges().pipe(map(changes => {
      return changes.map(change => ({
        id: change.payload.doc.id,
        ...change.payload.doc.data()
      }));
    }));
  }

  async createOrganizer(organizer: OrganizerItem) {
    await this.organizerItemsCollection.add(organizer);
  }

  async deleteOrganizer(organizer: OrganizerItem) {
    await this.afs.doc(`organizators/${organizer.id}`).delete();
  }

  async updateOrganizer(lastItem: OrganizerItem, newItem: OrganizerItem) {
    await this.afs.doc(`organizators/${lastItem.id}`).update(newItem);
  }
}
