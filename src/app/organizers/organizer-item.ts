export interface OrganizerItem {
  id?: string;
  name?: string;
  surname?: string;
  team?: Date;
  tel?: string;
}
