import { Component, OnInit } from '@angular/core';
import {OrganizersCreateComponent} from './organizers-create/organizers-create.component';
import {MatDialog} from '@angular/material';
import {OrganizersService} from './organizers.service';

@Component({
  selector: 'app-organizers',
  templateUrl: './organizers.component.html',
  styleUrls: ['./organizers.component.scss']
})
export class OrganizersComponent implements OnInit {
  organizers = [];
  constructor(private organizersService: OrganizersService, public dialog: MatDialog) { }

  ngOnInit() {
    this.organizersService.getItems().subscribe(organizers => {
      this.organizers = organizers;
    });
  }
  openForm(): void {
    const dialogRef = this.dialog.open(OrganizersCreateComponent, {
      width: '600px',
      data: {
        name: '',
        surname: '',
        team: '',
        tel: '',
      }
    });

    dialogRef.afterClosed().subscribe(async result => {
      if (result) {
       await this.organizersService.createOrganizer({...result});
      }
    });
  }
}
