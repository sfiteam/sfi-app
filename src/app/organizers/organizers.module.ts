import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizersComponent } from './organizers.component';
import {OrganizersRoutingModule} from './organizers-routing.module';
import { OrganizersCreateComponent } from './organizers-create/organizers-create.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {NgxAuthFirebaseUIModule} from 'ngx-auth-firebaseui';
import {MatSelectModule, MatTableModule} from '@angular/material';


@NgModule({
  declarations: [OrganizersComponent, OrganizersCreateComponent],
  imports: [
    CommonModule,
    OrganizersRoutingModule,
    CommonModule,
    FormsModule,
    SharedModule,
    NgxAuthFirebaseUIModule,
    MatTableModule,
    MatSelectModule,
  ],
  entryComponents: [OrganizersCreateComponent]
})
export class OrganizersModule {
}
