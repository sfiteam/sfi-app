import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {OrganizerItem} from '../organizer-item';


@Component({
  selector: 'app-organizers-create',
  templateUrl: './organizers-create.component.html',
  styleUrls: ['./organizers-create.component.scss']
})
export class OrganizersCreateComponent {
  teams = [
    {value: 'logistyka', viewValue: 'Logistyka'},
    {value: 'sponsorzy', viewValue: 'Sponsorzy'},
    {value: 'marketing', viewValue: 'Marketing'},
    {value: 'prelegenci', viewValue: 'Prelegenci'}
  ];
  constructor(
    public dialogRef: MatDialogRef<OrganizersCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: OrganizerItem) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
