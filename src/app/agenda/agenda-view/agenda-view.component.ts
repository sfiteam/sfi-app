import {Component, OnInit} from '@angular/core';
import {AgendaService} from '../agenda.service';
import {AgendaItem} from '../agenda-item';

import {MatDialog} from '@angular/material';
import {AgendaCreateComponent} from "../agenda-create/agenda-create.component";
import { AgendaEditComponent } from '../agenda-edit/agenda-edit.component';
import {FeedbackComponent} from '../feedback/feedback.component';

import { AuthService } from '../../auth/auth.service';


@Component({
  selector: 'app-agenda-view',
  templateUrl: './agenda-view.component.html',
  styleUrls: ['./agenda-view.component.scss']
})
export class AgendaViewComponent implements OnInit {
  lectures: AgendaItem[];

  constructor(private lectureService: AgendaService, public dialog: MatDialog, private auth: AuthService) {}

  openForm(): void {
    const dialogRef = this.dialog.open(AgendaCreateComponent, {
      width: '600px',
      data: {
        title: '',
        description: '',
        language: 'PL',
        room: '',
        speaker: '',
        date: null
      }
    });

    dialogRef.afterClosed().subscribe(async result => {
      if (result.title !== '') {
        await this.lectureService.createLecture({...result, rating: []});
      }
    });
  }

  openEditForm(item): void {

    const dialogRef = this.dialog.open(AgendaEditComponent, {
      width: '600px',
      data: {...item, date: item.date.toDate()}
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.updateItem(item, result);
    });
  }

  openFeedbackForm(item): void {

    const dialogRef = this.dialog.open(FeedbackComponent, {
      width: '600px',
      data: {
        rating: 3
      }
    });

    dialogRef.afterClosed().subscribe(async result => {
      await this.updateItem(item, {...item, rating: [...item.rating, result.rating]});
    });
  }

  ngOnInit() {
    this.lectureService.getItems().subscribe(lectures => {
      this.lectures = lectures;
    });
  }

  async deleteLecture(event, lecture) {
    await this.lectureService.deleteLecture(lecture);
  }

  async updateItem(lastItem, newItem) {
    await this.lectureService.updateLecture(lastItem, newItem);
  }

}
