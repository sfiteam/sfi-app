export interface AgendaItem {
    id?: string;
    title?: string;
    description?: string;
    date?: Date;
    speaker?: string;
    room?: string;
    language?: string;
    rating?: number[];
}
