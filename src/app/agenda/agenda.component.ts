import {Component} from "@angular/core";


@Component({
  selector: 'app-agenda',
  template: '<router-outlet></router-outlet>'
})
export class AgendaComponent {}
