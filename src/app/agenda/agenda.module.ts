import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AgendaComponent} from "./agenda.component";
import {AgendaRoutingModule} from "./agenda-routing.module";
import { AgendaViewComponent } from './agenda-view/agenda-view.component';
import {SharedModule} from "../shared/shared.module";
import {AgendaCreateComponent} from "./agenda-create/agenda-create.component";
import {FormsModule} from "@angular/forms";
import { AgendaEditComponent } from './agenda-edit/agenda-edit.component';
import {NgxAuthFirebaseUIModule} from 'ngx-auth-firebaseui';
import { FeedbackComponent } from './feedback/feedback.component';
import { EmojiModule } from '@ctrl/ngx-emoji-mart/ngx-emoji'

@NgModule({
  declarations: [AgendaComponent, AgendaViewComponent, AgendaCreateComponent, AgendaEditComponent, FeedbackComponent],
  imports: [
    CommonModule,
    AgendaRoutingModule,
    FormsModule,
    SharedModule,
    NgxAuthFirebaseUIModule,
    EmojiModule
  ],
  entryComponents: [AgendaCreateComponent, AgendaEditComponent, FeedbackComponent]
})
export class AgendaModule { }
