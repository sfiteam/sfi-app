import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {AgendaItem} from './agenda-item';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AgendaService {

  agendaItemsCollection: AngularFirestoreCollection<AgendaItem>;

  constructor(public afs: AngularFirestore) {
    this.agendaItemsCollection = this.afs.collection('lectures');
  }

  getItems() {
    return this.agendaItemsCollection.snapshotChanges().pipe(map(changes => {
      return changes.map(change => ({
        id: change.payload.doc.id,
        ...change.payload.doc.data()
      }));
    }));
  }

  async createLecture(lecture: AgendaItem) {
    await this.agendaItemsCollection.add(lecture);
  }

  async deleteLecture(lecture: AgendaItem) {
    await this.afs.doc(`lectures/${lecture.id}`).delete();
  }

  async updateLecture(lastItem: AgendaItem, newItem: AgendaItem) {
    await this.afs.doc(`lectures/${lastItem.id}`).update(newItem);
  }
}
