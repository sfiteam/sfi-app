import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AgendaItem} from '../agenda-item';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent {

  constructor(
    public dialogRef: MatDialogRef<FeedbackComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AgendaItem) {}

    onNoClick(): void {
      this.dialogRef.close();
    }

    setRate(rate): void {
      this.data.rating = rate 
    }

}
