import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AgendaItem} from '../agenda-item';

@Component({
  selector: 'app-agenda-edit',
  templateUrl: './agenda-edit.component.html',
  styleUrls: ['./agenda-edit.component.scss']
})
export class AgendaEditComponent {

  constructor(
    public dialogRef: MatDialogRef<AgendaEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AgendaItem) {}

    onNoClick(): void {
      this.dialogRef.close();
    }


}
