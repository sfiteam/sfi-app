import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AgendaItem} from '../agenda-item';


@Component({
  selector: 'app-agenda-create',
  templateUrl: './agenda-create.component.html',
  styleUrls: ['./agenda-create.component.scss']
})
export class AgendaCreateComponent {

  constructor(
    public dialogRef: MatDialogRef<AgendaCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AgendaItem) {}

    onNoClick(): void {
      this.dialogRef.close();
    }

}
