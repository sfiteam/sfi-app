import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {AgendaComponent} from "./agenda.component";
import {AgendaViewComponent} from "./agenda-view/agenda-view.component";

const routes: Routes = [
  {
    path: '',
    component: AgendaComponent,
    children: [
      {path: '', component: AgendaViewComponent},
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AgendaRoutingModule {}
