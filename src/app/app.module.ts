import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {CommonModule} from "@angular/common";
import {CoreModule} from "./core/core.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { AuthGuard } from './core/auth.guard';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    CoreModule,
  ],
  declarations: [AppComponent],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
  
}
