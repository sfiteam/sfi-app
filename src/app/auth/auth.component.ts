import { Component, OnInit } from '@angular/core';
import { AuthProvider } from 'ngx-auth-firebaseui';
import {Router} from "@angular/router"
import {AuthService} from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  providers = AuthProvider;
  
  constructor(private router: Router, public auth: AuthService) { }

  ngOnInit() {
  }

  successLogin() {
    this.router.navigate(['/']);
  }

}
