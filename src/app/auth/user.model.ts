export interface User {
    displayName?: string;
    email?: string;
    phoneNumber?: string;
    photoUrl?: string;
    providerId?: string;
    uid?: string;
}
