import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

import {auth} from 'firebase/app';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable, of} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {User} from './user.model';

declare var gapi: any;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$: Observable<User>;
  calendarItems: any[];

  constructor(
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
  ) {
    this.initClient();
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<any>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }

  // Google api calendar
  initClient() {
    gapi.load('client', () => {
      console.log('loaded client');

      gapi.client.init({
        apiKey: 'AIzaSyBLHkyVxYGC5nm-t47M9hgkGVtU5tE9go0',
        clientId: '247697111203-q4hn43iq0ftsnira9fmtpe0u1huk3u43.apps.googleusercontent.com',
        discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'],
        scope: 'https://www.googleapis.com/auth/calendar'
      });

      gapi.client.load('calendar', 'v3', () => console.log('loaded calendar'));
    });
  }

  async signOut() {
    await this.afAuth.auth.signOut();
    this.router.navigate(['/login']);
  }

  async login() {

    const googleAuth = gapi.auth2.getAuthInstance();
    const googleUser = await googleAuth.signIn();

    const token = googleUser.getAuthResponse().id_token;
    const credential = auth.GoogleAuthProvider.credential(token);

    await this.afAuth.auth.signInAndRetrieveDataWithCredential(credential)
      .then(() => {
        this.router.navigate(['/']);
      });

  }

  async getCalendar() {
    console.log(this.afAuth.auth.currentUser);
    const events = await gapi.client.calendar.events.list({
      calendarId: 'primary',
      timeMin: new Date().toISOString(),
      showDeleted: false,
      singleEvents: true,
      maxResults: 10,
      orderBy: 'startTime'
    });

    this.calendarItems = events.result.items;
  }

  async insertEvent(date, title, itemDesc) {

    if (this.afAuth.auth.currentUser.providerData[0].providerId !== 'google.com') {
      await this.login();
      return false;
    }
    await gapi.client.calendar.events.insert({
      calendarId: 'primary',
      start: {
        dateTime: startDate(date),
        timeZone: 'America/Los_Angeles'
      },
      end: {
        dateTime: endDate(date),
        timeZone: 'America/Los_Angeles'
      },
      summary: title,
      description: itemDesc
    });

    await this.getCalendar();
  }

}

const startDate = (time) => time.toDate().toISOString();
const endDate = (time) => new Date(time.toDate() + 1000 * 60 * 60).toISOString();
