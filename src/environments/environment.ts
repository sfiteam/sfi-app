// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyA6fXhE8qnpTOGIKDImE8OjI-fTvYIzC9c',
    authDomain: 'sfiapp-bbe02.firebaseapp.com',
    databaseURL: 'https://sfiapp-bbe02.firebaseio.com',
    projectId: 'sfiapp-bbe02',
    storageBucket: 'sfiapp-bbe02.appspot.com',
    messagingSenderId: '291495861465',
    appId: '1:291495861465:web:15f823fead70f4c9'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
